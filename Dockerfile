#DOCKERFILE
FROM centos:7
MAINTAINER joshuabister@gmail.com
### Atomic/OpenShift Labels - https://github.com/projectatomic/ContainerApplicationGenericLabels
LABEL name="jbister/mordhauserver" \
      maintainer="joshuabister@gmail.com" \
      vendor="Acme Corp" \
      version="1.2" \
      release="2" \
      summary="Container Mordhau Server" \
      description="Mordhau Server in a container" \
### Required labels above - recommended below
      run='docker run -tdi --name ${NAME} \
      -u 1001 \
      ${IMAGE}' \
      io.k8s.description="Mordhau Server in a container" \
      io.k8s.display-name="Mordhau_Server" \
      io.openshift.expose-services="7777,27015,15000" \
io.openshift.tags="mordhau,game_server,mordhau-arbitrary-uid,arbitrary,uid"
ARG APP_ROOT=/home/steam
ARG PORT=7777
ARG QUERY_PORT=27015
ARG BEACON_PORT=15000
ENV PATH=${APP_ROOT}/bin:${PATH}
EXPOSE ${PORT}/udp
EXPOSE ${QUERY_PORT}/udp
EXPOSE ${BEACON_PORT}/udp
  # libfontconfig1 libpangocairo-1.0-0 libnss3 libgconf2-4 libxi6 libxcursor1 libxss1 libxcomposite1 libasound2
  # libxdamage1 libxtst6 libatk1.0-0 libxrandr2
COPY ./config /tmp/config
RUN yum update -y \
    && yum install -y \
    	wget \
        net-tools \
    	glibc \
    	libstdc++ \
    	glibc.i686 \
    	libstdc++.i686 \
    	urw-fonts \
    	pango \
    	nss \
    	libconfig \
    	libXi \
    	libXcursor \
    	libXScrnSaver \
    	libXcomposite \
    	alsa-lib \
    	libXdamage \
    	libXtst \
    	atk \
    	libXrandr \ 
    && yum clean all \
    && useradd -m steam \
    && su steam -c \
        "cp /tmp/config/install ${APP_ROOT} \
        && cp /tmp/config/uid_entrypoint ${APP_ROOT} \
        && cd ${APP_ROOT} \
        && curl -sqL 'https://steamcdn-a.akamaihd.net/client/installer/steamcmd_linux.tar.gz' | tar zxvf - \
        && ls -la . \
        && ./steamcmd.sh +login anonymous +runscript install" 
USER steam
WORKDIR ${APP_ROOT}/mordhau
RUN ./MordhauServer.sh -port ${PORT} -QueryPort ${QUERY_PORT} -BeaconPort=${BEACON_PORT} & \
    sleep 15 \
    && kill %% \
    && cp /tmp/config/Config /home/steam/mordhau/Mordhau/LinuxServer -r
### user name recognition at runtime w/ an arbitrary uid - for OpenShift deployments
#ENTRYPOINT [ "$APP_ROOT/uid_entrypoint" ]
CMD ./MordhauServer.sh -port ${PORT} -QueryPort=${QUERY_PORT} -BeaconPort=${BEACON_PORT}
